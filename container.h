#ifndef __CONTAINER_H
#define __CONTAINER_H

#include <string>
#include "items.h"
using namespace std;

class Container : public Items {
	public:
		Container(string name, string description, double cost, double price, int max_scoops);
		string type() override;
		int max_scoops();

/*	string to_string();
	string get_container_name();
	int get_container_number();
	
	void add_serving(int n);

  private:
	string name;
	int number; */
	int _max_scoops;
};
#endif
