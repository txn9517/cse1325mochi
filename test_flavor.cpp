#include "test_flavor.h"
#include "flavor.h"
#include <iostream>

bool test_flavor() {
  std::string expected = "";
  bool passed = true; // Optimist!

  //
  // Test constructor
  //

  std::string x_name = "Fudge Ripple";
  std::string x_description = "Chocolatey goodness in vanilla swirl";
  double x_cost = 0.75;
  double x_price = 1.50;

  Scoop flavor{x_name, x_description, x_cost, x_price};

  if (flavor.name() != x_name ||
      flavor.description() != x_description ||
      flavor.cost() != x_cost ||
      flavor.price() != x_price ||
      flavor.type() != "Flavor" ||
      flavor.quantity() != 0) {
    std::cerr << "#### Flavor constructor fail" << std::endl;
    std::cerr << "Expected: " << x_name << ','
                              << x_description << ','
                              << x_cost << ','
                              << x_price << ','
                              << "Flavor" << ','
                              << '0' << std::endl;
    std::cerr << "Actual:   " << flavor.name() << ','
                              << flavor.description() << ','
                              << flavor.cost() << ','
                              << flavor.price() << ','
                              << flavor.type() << ','
                              << flavor.quantity() << std::endl;
    passed = false;
  }

  //
  // Report results
  //

  return passed;
}
