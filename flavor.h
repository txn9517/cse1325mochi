#ifndef __FLAVOR_H
#define __FLAVOR_H

#include <string> 
#include "items.h"
using namespace std;

class Flavor : public Items {
	public :
		Flavor(string name, string description, double cost, double price);
		string type() override;

/*	string  to_string();
	string get_flavor_name();
	int get_flavor_number();

	void add_serving(int n);

  private:
	string name;
	int number; */
};
#endif
