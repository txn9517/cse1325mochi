#include "container.h"
#include <string>
using namespace std;

Container::Container(string name, string description, double cost, double price, int max_scoops)
	: Items(name, description, cost, price), _max_scoops{max_scoops} { }

string Container::type() {
	return "Container";
}

int Container::max_scoops() {
	return _max_scoops;
}

/*string Container::to_string() { 
	std::string s = std::to_string(number);
	return s + " servings of " +name + ".\n";
}

string Container::get_container_name() {return name;}

void Container::add_serving(int n) {number= number + n;}

int Container::get_container_number() {return number;} */
