# Makefile for Mavs Ice Cream Emporium Software
CXXFLAGS += --std=c++11

test: items.o test_items.o test_flavor.o test_container.o test_topping.o flavor.o container.o topping.o
	$(CXX) $(CXXFLAGS) -o test1 items.o test_items.o test_flavor.o test_container.o test_topping.o flavor.o container.o topping.o

test_items: test_items.o items.o
	$(CXX) $(CXXFLAGS) -o test_items test_items.o items.o
test_items.o: test_items.cpp *.h
	$(CXX) $(CXXFLAGS) -c -w test_items.cpp
items: items.o
	$(CXX) $(CXXFLAGS) -o items items.o
items.o: items.cpp *.h
	$(CXX) $(CXXFLAGS) -c items.cpp

test_flavor: test_flavor.o flavor.o
	$(CXX) $(CXXFLAGS) -o test_flavor test_flavor.o flavor.o
test_flavor.o: test_flavor.cpp *.h
	$(CXX) $(CXXFLAGS) -c -w test_flavor.cpp
flavor.o: flavor.cpp *.h
	$(CXX) $(CXXFLAGS) -c flavor.cpp

test_container: test_container.o container.o
	$(CXX) $(CXXFLAGS) -o test_container test_container.o container.o
test_container.o: test_container.cpp *.h
	$(CXX) $(CXXFLAGS) -c -w test_container.cpp
container.o: container.cpp *.h
	$(CXX) $(CXXFLAGS) -c container.cpp

test_topping: test_topping.o topping.o
	$(CXX) $(CXXFLAGS) -o test_topping test_topping.o topping.o
test_topping.o: test_topping.cpp *.h
	$(CXX) $(CXXFLAGS) -c -w test_topping.cpp
topping.o: topping.cpp *.h
	$(CXX) $(CXXFLAGS) -c topping.cpp

clean:
	-rm -f *.o test1 items test_items test_flavor test_container test_topping

