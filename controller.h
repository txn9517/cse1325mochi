#ifndef _CONTROLLER_H
#define _CONTROLLER_H 

#include "emporium.h"
#include "view.h"

class Controller {
	public:
		Controller (Emporium& emp) : emporium(emp), view(View(emporium)) { }
		void cli();
		void execute_cmd(int cmd);
	private:
		Emporium& emporium;
		

