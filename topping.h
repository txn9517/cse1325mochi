#ifndef __TOPPING_H
#define __TOPPING_H

#include <string>
#include "items.h"
using namespace std;

class Topping : public Items {
	public:
		Topping(string name, string description, double cost, double price);
		string type() override;

/*	string to_string();
	string get_topping_name();
	int get_topping_number();
	
	void add_serving(int n);

  private:
	string name;
	int number; */
};
#endif
