#ifdef __EMPORIUM_H
#define __EMPORIUM_H

#include <iostream>

using namespace std;

class Emporium {
	public:
			void load(istream ist);
			void save(ostream ost);
	
	private:
		Item items;
		Order orders;
		User users;
		double cash_register = 0;
};

#endif