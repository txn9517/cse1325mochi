#include "test_items.h"
#include "test_container.h"
#include "test_flavor.h"
#include "test_topping.h"
#include <iostream>

int main() {
  if (!(test_item() &&
        test_container() &&
        test_flavor() &&
        test_topping()))
    std::cerr << "fail" << std::endl;
} 
