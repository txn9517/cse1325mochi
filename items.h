#ifndef ITEMS_H
#define ITEMS_H
#include <string>
using namespace std;

class Items {
	public:
		Items(string name, string description, double cost, double price);
		void restock(int quantity = 25);
		void consume(int quantity = 1);
		virtual string type();
		string name();
		string description();
		double cost();
		double price();
		int quantity();

	private:
		string _name;
		string _description;
		double _cost;
		double _price;
		int _quantity;
};
#endif
