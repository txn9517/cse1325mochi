#include "items.h"

Items::Items(string name, string description, double cost, double price) :
	_name{name}, _description{description}, _cost{cost}, _price{price}, _quantity{0} { }

std::string Items::type() {
	return "Item";
}

void Items::restock(int quantity) {
	_quantity = quantity;
}

void Items::consume(int quantity) {
	_quantity -= quantity;
}

string Items::name() {
	return _name;
}

string Items::description() {
	return _description;
}

double Items::cost() {
	return _cost;
}

double Items::price() {
	return _price;
}

int Items::quantity() {
	return _quantity;
}
