#include "topping.h"
#include <string>
using namespace std;

Topping::Topping(string name, string description, double cost, double price)
	: Items(name, description, cost, price) { }

string Topping::type() {
	return "Topping";
}

/*string Topping::to_string(){
	std::string s = std::to_string(number);
	return s + " servings of " +name + ".\n";}

string Topping::get_topping_name() {return name;}
int Topping::get_topping_number() {return number;}

void Topping::add_serving(int n) {number = number+n;} */
